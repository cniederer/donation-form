var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function ProgressText(props) {
  if (props.dollarsRemaining > 0) {
    return React.createElement(
      'div',
      { className: 'progress-text' },
      React.createElement(
        'span',
        null,
        React.createElement(
          'strong',
          null,
          React.createElement(
            'sup',
            null,
            '$'
          ),
          props.dollarsRemaining.toLocaleString()
        ),
        ' still needed to fund this project'
      )
    );
  } else {
    return React.createElement(
      'div',
      { className: 'progress-text' },
      React.createElement(
        'span',
        null,
        'This project has been fully funded! Thank you!'
      )
    );
  }
}

function ProgressBar(props) {
  if (props.dollarsRemaining > 0) {
    var dollarsDonated = props.goal - props.dollarsRemaining;
    var percentageRemaining = dollarsDonated / props.goal * 100;

    var styles = {
      width: percentageRemaining + '%'
    };

    return React.createElement(
      'div',
      { className: 'progress-bar-container' },
      React.createElement('div', { className: 'progress-bar', style: styles })
    );
  } else {
    return React.createElement(
      'div',
      { className: 'progress-bar-container' },
      React.createElement('div', { className: 'progress-bar', style: { 'width': '100%' } })
    );
  }
}

function DonorCount(props) {
  if (props.donorCount === 0) {
    return React.createElement(
      'p',
      null,
      'Be the first to donate!'
    );
  } else if (props.donorCount === 1) {
    return React.createElement(
      'p',
      null,
      'Join the ',
      React.createElement(
        'strong',
        null,
        props.donorCount.toLocaleString()
      ),
      ' other donor who has already supported this project.'
    );
  } else {
    return React.createElement(
      'p',
      null,
      'Join the ',
      React.createElement(
        'strong',
        null,
        props.donorCount.toLocaleString()
      ),
      ' other donors who have already supported this project.'
    );
  }
}

var DonationForm = function (_React$Component) {
  _inherits(DonationForm, _React$Component);

  function DonationForm(props) {
    _classCallCheck(this, DonationForm);

    // goal is 5000, we'll use it to calculate dollarsRemaining
    var _this = _possibleConstructorReturn(this, (DonationForm.__proto__ || Object.getPrototypeOf(DonationForm)).call(this, props));

    var goal = 5000;

    // dollarsRemaining matches the goal at the start of DonationForm's lifecycle
    _this.state = {
      dollarsRemaining: goal,
      donationValue: '',
      donorCount: 0,
      goal: goal,
      thankYouMessage: ''
    };

    _this.handleFocus = _this.handleFocus.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);
    _this.handleSubmit = _this.handleSubmit.bind(_this);
    return _this;
  }

  _createClass(DonationForm, [{
    key: 'handleFocus',
    value: function handleFocus() {
      this.setState({ thankYouMessage: '' });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(event) {
      this.setState({ donationValue: event.target.value, thankYouMessage: '' });
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit(event) {
      event.preventDefault();
      this.setState(function (state) {
        return {
          dollarsRemaining: state.dollarsRemaining - state.donationValue,
          donationValue: '',
          donorCount: state.donorCount + 1,
          thankYouMessage: 'Thank you for your donation!'
        };
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var dollarsRemaining = this.state.dollarsRemaining;
      var donorCount = this.state.donorCount;
      var goal = this.state.goal;
      var thankYouMessage = this.state.thankYouMessage;

      return React.createElement(
        'div',
        { className: 'donation-container' },
        React.createElement(ProgressText, { dollarsRemaining: dollarsRemaining }),
        React.createElement(ProgressBar, { dollarsRemaining: dollarsRemaining, goal: goal }),
        React.createElement(
          'div',
          { className: 'donation-inner' },
          React.createElement(
            'h1',
            null,
            'Only four days left to fund this project'
          ),
          React.createElement(DonorCount, { donorCount: donorCount }),
          React.createElement(
            'form',
            { onSubmit: this.handleSubmit },
            React.createElement(
              'span',
              { className: 'success-message' },
              thankYouMessage
            ),
            React.createElement(
              'label',
              { htmlFor: 'donationvalue', className: 'visually-hidden' },
              'Please enter your donation'
            ),
            React.createElement('input', { name: 'donationvalue', id: 'donationvalue', type: 'number', min: '5', step: '0.01', required: true, value: this.state.donationValue, onChange: this.handleChange, onFocus: this.handleFocus }),
            React.createElement(
              'button',
              { type: 'submit' },
              'Give Now'
            )
          )
        )
      );
    }
  }]);

  return DonationForm;
}(React.Component);

ReactDOM.render(React.createElement(DonationForm, null), document.getElementById('root'));