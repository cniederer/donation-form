# Donation Form

Donation form for an upcoming event. Built using React and SCSS.

[Link to CodePen to see it in action](https://codepen.io/niederer/pen/YzGBywK)
