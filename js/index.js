function ProgressText(props) {
  if (props.dollarsRemaining > 0) {
    return (
      <div className='progress-text'>
        <span><strong><sup>$</sup>{props.dollarsRemaining.toLocaleString()}</strong> still needed to fund this project</span>
      </div>
    )
  } else {
    return (
      <div className='progress-text'>
        <span>This project has been fully funded! Thank you!</span>
      </div>
    )
  }
}

function ProgressBar(props) {
  if (props.dollarsRemaining > 0) {
    let dollarsDonated = (props.goal - props.dollarsRemaining);
    let percentageRemaining = ((dollarsDonated / props.goal) * 100);

    let styles = {
      width: `${percentageRemaining}%`
    }

    return (
      <div className='progress-bar-container'>
        <div className='progress-bar' style={styles}></div>
      </div>
    )
  } else {
    return (
      <div className='progress-bar-container'>
        <div className='progress-bar' style={{'width': '100%'}}></div>
      </div>
    )
  }
}

function DonorCount(props) {
  if (props.donorCount === 0) {
    return (
      <p>Be the first to donate!</p>
    )
  } else if (props.donorCount === 1) {
    return (
      <p>Join the <strong>{props.donorCount.toLocaleString()}</strong> other donor who has already supported this project.</p>
    )
  } else {
    return (
      <p>Join the <strong>{props.donorCount.toLocaleString()}</strong> other donors who have already supported this project.</p>
    )
  }
}

class DonationForm extends React.Component {
  constructor(props) {
    super(props);

    // goal is 5000, we'll use it to calculate dollarsRemaining
    const goal = 5000;

    // dollarsRemaining matches the goal at the start of DonationForm's lifecycle
    this.state = {
      dollarsRemaining: goal,
      donationValue: '',
      donorCount: 0,
      goal: goal,
      thankYouMessage: ''
    }

    this.handleFocus = this.handleFocus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleFocus() {
    this.setState({ thankYouMessage: '' })
  }

  handleChange(event) {
    this.setState({ donationValue: event.target.value, thankYouMessage: '' });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState((state) => ({
      dollarsRemaining: (state.dollarsRemaining - state.donationValue),
      donationValue: '',
      donorCount: (state.donorCount + 1),
      thankYouMessage: 'Thank you for your donation!'
    }));
  }

  render() {
    let dollarsRemaining = this.state.dollarsRemaining;
    let donorCount = this.state.donorCount;
    let goal = this.state.goal;
    let thankYouMessage = this.state.thankYouMessage;

    return (
      <div className='donation-container'>
        <ProgressText dollarsRemaining={dollarsRemaining} />
        <ProgressBar dollarsRemaining={dollarsRemaining} goal={goal} />
        <div className='donation-inner'>
          <h1>Only four days left to fund this project</h1>
          <DonorCount donorCount={donorCount} />
          <form onSubmit={this.handleSubmit}>
            <span className='success-message'>{thankYouMessage}</span>
            <label htmlFor='donationvalue' className='visually-hidden'>Please enter your donation</label>
            <input name='donationvalue' id='donationvalue' type='number' min='5' step='0.01' required value={this.state.donationValue} onChange={this.handleChange} onFocus={this.handleFocus} />
            <button type='submit'>Give Now</button>
          </form>
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <DonationForm />,
  document.getElementById('root')
);
